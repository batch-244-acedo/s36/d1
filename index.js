// Express JS Server

// set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoutes");

// server setup
const app = express();
const port = 3001;
// middlewares
app.use(express.json());
	// allows your app to read json data
app.use(express.urlencoded({extended:true}));
	// allows your app to read data from forms,by applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application


// database connection
mongoose.connect("mongodb+srv://marcokalalo:admin@wdc028-course-booking.hrvki.mongodb.net/b244-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
			// { newUrlParser : true } allows us to avoid any current and future errors while connecting to MongoDB
		useUnifiedTopology : true
			// fixing deprection warnings
	}
);

//allows all the task routes created in the taskRoutes.js to use "/tasks" route
app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}`))
