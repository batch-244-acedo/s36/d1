// routes - contains all the endpoints for our applications

const express = require("express");
const router = express.Router();
	// allows access to HTTP method (get, put, post, delete) middlewares that makes it easier to create routes for our application

// taskController allows us to use the functions defined in the taskController.js file
const taskController = require("../controllers/taskController");

// route to all get the tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// route to delete a task
// ":id: is a wildcard
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
	//params - URL parameter
})

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/complete", (req, res) => {
	taskController.updateSpecificStatus(req.params.id).then(resultFromController => res.send(resultFromController));
})

// export the module
module.exports = router;